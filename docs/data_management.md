# Gestion des fichiers

## Introduction

ANIS fournit des jeux de données extraits de bases de données métiers.
ANIS permet d'associer des fichiers (fits, png, xml, ...) aux jeux de données.
Nous allons voir ici comment associer des données à des jeux des données.

## Répertoire racine des données

Tout d'abord, vous devez spécifier le répertoire dans lequel les données seront stockées.
Ce répertoire deviendra le répertoire racine du serveur ANIS.
Allez dans le fichier `docker-compose.yml` pour configurer la variable d'environnement `DATA_PATH`.

![anis_server_data_path](img/anis_server_data_path.png)

Ici, vous pouvez voir que le répertoire de données est fixé à `/data`.

## Explorer les fichiers

Un administrateur peut lister les fichiers ou dossiers contenus dans le répertoire racine.
Veuillez noter que si l'autorisation est activée, vous devrez envoyer un jeton pour effectuer cette action.

Si vous voulez explorer le répertoire racine d'ANIS, vous pouvez faire :

```bash
curl http://localhost:8080/file-explorer
```

```js
[{
	"name": ".",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "..",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "DEFAULT",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}]
```

Si vous voulez explorer le répertoire `DEFAULT`, tapez : 

```bash
curl http://localhost:8080/file-explorer/DEFAULT
```

```js
[{
	"name": ".",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "..",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "ASPIC",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "IRIS",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "favicon.ico",
	"size": 4286,
	"type": "file",
	"mimetype": "image\/vnd.microsoft.icon"
}, {
	"name": "home_component_logo.png",
	"size": 53645,
	"type": "file",
	"mimetype": "image\/png"
}, {
	"name": "logo.png",
	"size": 3290,
	"type": "file",
	"mimetype": "image\/png"
}]
```

## Associer des fichiers à un projet scientifique

Vous pouvez associer un répertoire de données pour un projet scientifique.
Le chemin du répertoire commencera à partir du répertoire racine des données de anis-server.

Par exemple pour le projet `DEFAULT` vous pouvez associer le répertoire `DEFAULT`.
Le chemin complet sera alors : `/data/DEFAULT`

Pour configurer le chemin des données d'un projet scientifique, vous devez remplir la propriété `data_path` 
de l'instance qui représente votre projet scientifique.

Pour ce faire : 

1. Rendez-vous dans l'interface d'administration : http://localhost:4200/admin
2. Editer l'instance `Default instance` déjà présente
3. Editer la propriété `data_path` pour ajouter le répertoire `DEFAULT` si il n'est pas déjà renseigné

## Associer des fichiers à un jeu de données

Vous pouvez associer un répertoire de données pour chacun des jeux de données.
Le chemin du répertoire commencera à partir du répertoire racine des données du projet scientifique dans lequel le jeu de données est ajouté.

Par exemple pour le jeu de données `IRiS obs` vous pouvez associer le répertoire `IRIS/observations`
Le chemin complet sera alors `/data/DEFAULT/IRIS/observations`.

Pour configurer le chemin des données d'un jeu de données, vous devez remplir la propriété `data_path`.

Pour ce faire : 

1. Rendez-vous dans l'interface d'administration : http://localhost:4200/admin
2. Configurer l'instance `Default`
3. Editer le jeu de données `IRiS obs`
4. Editer la propriété `data_path` pour ajouter le répertoire `IRIS/observations` si il n'est pas déjà renseigné

## Explorer les fichiers pour un jeu de données

Un utilisateur peut lister les fichiers ou dossiers contenus dans le répertoire de l'ensemble de données.
Veuillez noter que si le jeu de données est privé, vous devrez envoyer un jeton pour effectuer cette action.

Exemple avec le dataset `IRiS obs` : 

```bash
$ curl http://localhost:8080/dataset-file-explorer/observations/20141002
```

```js
[{
	"name": ".",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "..",
	"size": 4096,
	"type": "dir",
	"mimetype": "directory"
}, {
	"name": "M_15-S001-R001-C001-SDSS_g.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_15-S001-R001-C001-SDSS_g_dupe-1.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_15-S001-R001-C001-SDSS_g_dupe-2.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_15-S001-R001-C001-SDSS_g_dupe-3.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_15-S001-R001-C001-SDSS_g_dupe-4.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_15-S001-R001-C001-SDSS_g_dupe-5.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_31-S001-R001-C001-SDSS_g.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_39-S001-R001-C001-SDSS_g.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}, {
	"name": "M_81-S001-R001-C001-SDSS_g.fits",
	"size": 8395200,
	"type": "file",
	"mimetype": "image\/fits"
}]
```

## Télécharger un fichier

Un utilisateur peut télécharger un fichier associé à un jeu de données.
Veuillez noter que si le jeu de données est privé, vous devrez envoyer un jeton pour effectuer cette action.

Exemple : 

```bash
wget http://localhost:8080/download-file/observations/20141002/M_15-S001-R001-C001-SDSS_g.fits
```
