# Installation

## Introduction

Ici nous allons voir les différentes étapes pour installer le logiciel en local sur notre machine.
Les exemples de cette documentation sont effectués avec un système Linux.

ANIS est composé d'un monorepo qui contient l'ensemble des sous entités, c'est à dire le `server`, le `client` et les `services`.

## Dépôt Git

L'ensemble du code source d'ANIS est disponible dans un dépôt publique sur le gitlab du LAM.

Nous allons tout d'abord cloner le projet ANIS sur notre machine en local en utilisant l'adresse du dépôt, comme ceci :

```bash
$ git clone git@gitlab.lam.fr:anis/anis-next.git
$ cd anis-next
```

## Makefile

ANIS contient un fichier `Makefile` qui va aider le développeur à installer et démarrer l'application sur sa machine.
Pour pouvoir lister les opérations disponibles vous pouvez simplement taper dans votre terminal la commande suivante : 

```bash
$ make
```

![anis_make](img/anis_make.png)

## Dépendances

### Installation des dépendances du client

Le client est une application web écrite en Angular/Typescript et qui requiert des dépendances pour fonctionner.
Pour installer les dépendances du client, vous devez taper dans votre terminal la commande suivante :

```bash
$ make install_client
```

Les dépendances du client vont se télécharger et s'installer dans un dossier nommé `client/node_modules`. Cette opération peut prendre quelques minutes.

Pour votre information, vous pouvez trouver la liste des dépendances dans le fichier `client/package.json`.

### Installation des dépendances du serveur

Le serveur est une application écrite en PHP et qui requiert également des dépendances pour fonctionner.
Pour installer les dépendances du serveur, vous devez taper dans votre terminal la commande suivante :

```bash
$ make install_server
```

Les dépendances du serveur vont se télécharger et s'installer dans un dossier nommé `server/vendor`. Cette opération peut prendre quelques minutes.

Pour votre information, vous pouvez trouver la liste des dépendances dans le fichier `server/composer.json`.

### Récupération des fichiers associés

Pour fonctionner ANIS a également besoin d'un répertoire pour les fichiers associès. 

Dans la version en développement, nous fournissons une archive contenant des fichiers prêt à l'emploi.
Pour installer ce répertoire, vous devez remonter d'un niveau dans l'arborescence, télécharger l'archive et la décompresser : 

Comme ceci : 

```bash
$ cd ..
$ wget https://anis.lam.fr/files/anis-data.tar.gz
$ tar xzvf anis-data.tar.gz
$ rm anis-data.tar.gz
$ cd anis-next
```

## Démarrage de l'application

Il est maintenant temps de démarrer l'application ANIS. Pour ce faire, taper la commande suivante :

```bash
$ make start
```

Cette commande va utiliser le fichier `docker-compose.yml` pour créer et lancer les différents conteneurs nécessaires à ANIS.
Ceci peut prendre quelques minutes. Vous pouvez regarder le status des conteneurs ANIS en tapant la commande suivante : 

```bash
$ make status
```

Vous pouvez également suivre les logs : 

```bash
$ make logs
```

## Bases de données

Pour fonctionner ANIS à besoin d'au moins deux bases de données :

 - Une base de données pour stocker la configuration d'ANIS
 - Une base de données contenant les jeux de données métiers à partager

Dans la version en développement, nous fournissons deux bases de données prêt à l'emploi.
Pour installer la base de données de configuration avec des jeux de données de tests, taper la commande suivante :

```bash
$ make create-db
```

## ANIS est maintenant prêt

### Le serveur

* Le serveur est disponible à l'adresse suivante : `http://localhost:8080`

![anis_server_root](img/anis_server_root.png)

Quelques exemples de requêtes au serveur :

* Pour lister les projets scientifiques disponibles : `http://localhost:8080/instance`
* Pour lister les jeux de données disponibles pour le projet par défaut : `http://localhost:8080/instance/default/dataset`
* Pour afficher toutes les données pour le jeux de données `observations` avec les colonnes 1, 2 et 3 : `http://localhost:8080/search/observations?a=1;2;3`
* Pour compter les données disponibles pour le jeux de données `observations` : `http://localhost:8080/search/observations?a=count`
* Pour afficher seulement 3 enregistrements du jeux de données (recherche par ID) `observations` : `http://localhost:8080/search/observations?a=1;2;3&c=1::in::418|419|420`

### Le client web

* Le client web est disponible à l'adresse suivante : `http://localhost:4200`

### Les services

* Les services sont disponibles à l'adresse suivante : `http://localhost:5000`