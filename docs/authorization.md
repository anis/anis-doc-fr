# Autorisation

## Introduction

Certaines requêtes vers le serveur nécessitent une autorisation particulière.
Nous allons expliquer dans cette partie comment ANIS gére les autorisations.

## JWT

ANIS utilise un standard du web pour gérer les autorisations : `JWT`.
Vous pouvez trouver plus d'informations sur JWT en allant sur le site officiel : [https://jwt.io](https://jwt.io).

JWT est un systéme d'authentification qui utilise des jetons fournis à l'utilisateur pour prouver son idendité ainsi que ces droits d'accès.
Le jeton fourni est une chaine de caractères encodée en base64.

L'utilisateur doit envoyer un jeton JWT au serveur ANIS, à chaque requête serveur nécessitant une autorisation, pour prouver son identité.

![encoded-jwt](img/encoded-jwt.png#center)

Le jeton est composé de trois parties séparées par un point : 

 - En rouge -> l'en-tête
 - En violet -> la charge utile
 - En bleu -> la signature numérique du jeton

### L'en-tête

L'entête est une chaîne de caractères encodée en base64 que vous pouvez décoder.
La chaîne décodée est un JSON qui contient des informations sur la façon de valider le jeton, comme par exemple l'algorithme utilisé pour 
la signature numérique.

![header-jwt](img/header-jwt.png#center)

### La charge utile

La seconde partie est aussi une chaîne de caractères encodée en base64 que vous pouvez décoder.
C'est un JSON qui contient la charge utile. Le JSON peut contenir des informations sur le jeton mais aussi 
des informations ajoutées par l'application ANIS comme les rôles de l'utilisateur par exemple.

![header-jwt](img/payload-jwt.png#center)

### La signature

La dernière partie contient la signature numérique du jeton qui est ajoutée par l'application de fabrication des jetons.
Cette signature permet de s'assurer de l'authenticité du jeton. Dans notre exmple, la signature est fabriquée comme ceci :

![signature-jwt](img/signature-jwt.png#center)

La signature est utilisée pour vérifier que le message n'a pas été modifié avant dêtre envoyé avec la requête. 
Elle peut également servir à vérifier que l'expéditeur du JWT est bien celui qu'il prétend être.

## Activer l'autorisation dans ANIS

Par défaut l'autorisation n'est pas activé dans ANIS. Cela signifie que les requêtes vers le serveur ne sont pas protégées.
Pour activer l'autorisation vous devez changer une variable d'environnement dans la configuration.
Vous pouvez trouver la configuration de ANIS serveur dans le fichier `docker-compose.yml` à la racine du projet.

![php_settings.png](img/php_settings.png#center)

Voici la liste des options concernant la partie autorisation : 

- `TOKEN_ENABLED`: `0` signifie que l'autorisation est désactivé et `1` qu'ell est activé
- `TOKEN_PUBLIC_KEY_FILE`: Chemin vers le fichier contenant la clé publique qui sera utilisée pour vérifier l'authenticité du jeton.
- `TOKEN_ADMIN_ROLE`: Rôle utilisateur utilisé pour la partie administration de ANIS

## Envoyer le jeton avec une requête

Pour prouver son identité un utilisateur doit transmettre un jeton JWT avec sa requête.
Le jeton JWT va ensuite être validé par le serveur ANIS est les informations de la charge utile seronts utilisés.
L'utilisateur doit transmettre le token avec le Header HTTP Authorization comme ceci :

```
Authorization: Bearer <token>
```

Exemple avec curl :

```bash
curl http://localhost:8080/database --header 'Authorizarion: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
```

## Keycloak

ANIS ne génère pas les jetons JWT lui même et il va déléguer cette tâche à un logiciel tiers : `Keycloak`.

Keycloak est un logiciel open source permettant une authentification unique avec une gestion des identités et des accès destinée aux applications et services modernes.
Keycloak va donc générer des jetons JWT pour nous.
Pour plus d'informations vous pouvez visiter le site officiel : [https://www.keycloak.org/](https://www.keycloak.org/).

ANIS inclut par défaut une installation keycloak pour le développement dans un conteneur.
Vous pouvez trouver la configuration du conteneur keycloak dans le fichier `docker-compose.yml`.

Concrètement, un utilisateur va demander une connexion vers keycloak pour obtenir un jeton JWT. L'utilisateur devra ensuite transmettre ce jeton 
aux requêtes ANIS serveur qui nécessitent une autorisation.

Vous pouvez vous connecter sur l'interface d'administration du keycloak local en allant à l'adresse : [http://localhost:8180](http://localhost:8180).
Le nom d'utilisateur est `admin` et le mot de passe `admin`. 

## Connecter ANIS à Keycloak

Par défaut la version en développement de ANIS est connecté sur le keycloak en local (container docker).

Si vous souhaitez connecter ANIS sur une autre installation de Keycloak, par exemple en mode production, vous devez changer les variables d'environnements pour 
le conteneur serveur.

Voici la liste des options concernant la partie connexion à keycloak :

- `SSO_AUTH_URL`: Adresse du serveur Keycloak utilisé
- `SSO_REALM`: Nom du royaume utilisé
- `SSO_CLIENT_ID`: Nom du client Keycloak utilisé

## Clé publique

ANIS a besoin de la clé publique utilisée par keycloak pour vérifier les jetons envoyés par les utilisateurs. La clé doit donc être mis à jour lors d'une 
nouvelle installation comme ceci :

1. Ouvrez un navigateur est rendez-vous sur : [http://localhost:8180](http://localhost:8180)
2. Cliquez sur le lien `Administration console`
3. Entrez le nom d'utilisateur et le mot de passe (par défault = admin/admin)
4. Dans la partie `realms settings` selectionné l'onglet `Keys`
5. Dans le premier enregistrement cliquez sur le bouton `Public key`
6. Copiez la clé
7. Dans la répertoire `anis-server` du projet, rendez-vous dans le fichier `conf-dev/public_key`
8. Collez la clé entre le `BEGIN` et `END` (mettre à jour si la clé éxiste déjà)

![keycloak_public_key.png](img/keycloak_public_key.png#center)

## Compte utilisateur dans ANIS

Chaque visiteur peut créer un compte à partir de l'interface web client.

1. Ouvrez un navigateur et rendez-vous dans ANIS client : [http://localhost:4200](http://localhost:4200)
2. Cliquez sur le bouton `Sign In / Register`
3. Cliquez sur le bouton `Register`
4. Remplissez le formulaire et cliquez sur `register`
5. Rendez-vous sur l'adresse du `mail catcher` [http://localhost:1080](http://localhost:1080) pour lire le mail envoyé par keycloak et valider votre nouveau compte
6. Vous êtes maintenant connecté à votre compte utilisateur

![anis_register.png](img/anis_register.png#center)

## Client confidentiel

Par défaut, l'installation locale de keycloak enregistre un client confidentiel nommé anis-server.
Nous pouvons donc demander un jeton à keycloak via ce client. Mais d'abord nous devons configurer un secret pour notre client keycloak anis-server.

1. Ouvrez un navigateur et rendez-vous sur : [http://localhost:8180](http://localhost:8180)
2. Cliquez sur le lien `Administration console`
3. Entrez le nom d'utilisateur et le mot de passe (par défault = admin/admin)
4. Click sur le lien`Clients`
5. Sélectionnez  `anis-server`
6. Sélectionnez l'onglet `Credentials`
7. Cliquez sur le bouton `Regenerate Secret`

![confidential_client_secret.png](img/confidential_client_secret.png#center)

Vous pouvez maintenant envoyer une requête à keycloak pour obtenir un jeton :

```bash
curl -k --data "grant_type=client_credentials&client_id=anis-server&client_secret=**********" http://localhost:8180/auth/realms/anis/protocol/openid-connect/token
```

Remplacez `**********` avec le secret obetnu depuis l'interace keyclaok.

![confidential_client_secret.png](img/confidential_client_get_token.png#center)

Le jeton à envoyer à ANIS est le `access_token`. Il est par défaut valide pendant cinq minutes après sa génération.
Ce jeton généré contient le rôle `anis_admin`. Vous pouvez donc l'utiliser pour des opérations d'administration.
