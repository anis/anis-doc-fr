# API Serveur

## Introduction

Le serveur Anis fournit une API REST pour effectuer des actions. 
Dans cette section, nous allons énumérer les différentes URL possibles.

Vous pouvez trouver la liste de toutes les routes possibles (URL) dans le fichier `app/routes.php`.
Chaque route fait référence à une classe `Action` et lorsque une URL est appelée c'est la fonction `__invoke` qui est exécutée.

## Actions

Vous pouvez trouver les fichiers `Action` dans le dossier `src/Action`.
Vous pouvez modifier une ou plusieurs actions pour changer le comportement d'un point d'entrée.
Vous pouvez également ajouter un fichier d'action pour ajouter un point d'entrée.

Si vous voulez ajouter une nouvelle action, vous devez déclarer une nouvelle route dans le fichier
`app/routes.php` et vous devez instancier votre nouvelle `Action` dans le fichier `app/dependencies.php`.

## URLs

### Root 

Utilisé pour s'assurer que ANIS serveur fonctionne.

**URL** : `/`

**Method** : `GET`

**URL Params** : None

**Query Params** : None

**Auth required** : No

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `200 OK`

**Exemple de contenu**

```json
{
    "message": "it works!"
}
```

### Récupérer la liste des bases de données

Utilisé pour récupérer la liste des bases de données métiers disponibles.

**URL** : `/database`

**Method** : `GET`

**URL Params** : 

**Query Params** : None

**Auth required** : Yes

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `200 OK`

**Exemple de contenu**

```json
[
    {
        "id": 1,
        "label": "Test",
        "dbname": "anis_test",
        "dbtype": "pdo_pgsql",
        "dbhost": "db",
        "dbport": 5432,
        "dblogin": "anis",
        "dbpassword": "anis"
    }
]
```

### Ajouter une base de données

Utilisé pour ajouter une base de données métier.

**URL** : `/database`

**Method** : `POST`

**URL Params** : None

**Query Params** : None

**Body** : 

| Name       | Description                                                 | Type   | Available |
|------------|-------------------------------------------------------------|--------|:---------:|
| label      | Label de la nouvelle base de données                        | string |     X     |
| dbname     | Nom de la base de données                                   | string |     X     |
| dbtype     | Driver Doctrine utilisé (pdo_pgsql, pdo_mysql, ...)         | string |     X     |
| dbhost     | Nom du serveur hôte de la base de données                   | string |     X     |
| dbport     | Port d'écoute                                               | int    |     X     |
| dblogin    | Nom d'utilisateur de connexion                              | string |     X     |
| dbpassword | Mot de passe de connexion                                   | string |     X     |

**Auth required** : Yes

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `201 Created`

**Exemple de contenu**

```json
[
    {
        "id": 1,
        "label": "Test",
        "dbname": "anis_test",
        "dbtype": "pdo_pgsql",
        "dbhost": "db",
        "dbport": 5432,
        "dblogin": "anis",
        "dbpassword": "anis"
    }
]
```

<p class='error_response' markdown='1'>Error Response : </p>

**Code** : `400 Bad Request`

**Exemple de contenu**

```json
{
    "message": "400 Bad Request",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpBadRequestException",
            "code": 400,
            "message": "Param dbpassword needed to add a new database",
            "file": "/project/src/Action/DatabaseListAction.php",
            "line": 50
        }
    ]
}
```

### Récupérer une base de données par ID

Utilisé pour récupérer une base de données métier par son ID.

**URL** : `/database/{id}`

**Method** : `GET`

**URL Params** : 

| Name       | Description                | Type   | Available |
|------------|----------------------------|--------|:---------:|
| id         | ID de la base de données   | int    |     X     |

**Query Params** : None

**Auth required** : Yes

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `200 OK`

**Exemple de contenu**

```json
{
    "id": 1,
    "label": "Test",
    "dbname": "anis_test",
    "dbtype": "pdo_pgsql",
    "dbhost": "db",
    "dbport": 5432,
    "dblogin": "anis",
    "dbpassword": "anis"
}
```

<p class='error_response' markdown='1'>Error Response : </p>

**Code** : `404 Not Found`

**Exemple de contenu**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 45
        }
    ]
}
```

### Editer une base de données

Permet d'éditer les informations d'une base de données métier.

**URL** : `/database/{id}`

**Method** : `PUT`

**URL Params** :

| Name       | Description                 | Type   | Available |
|------------|-----------------------------|--------|:---------:|
| id         | ID de la base de données    | int    |     X     |

**Query Params** : None

**Body** : 

| Name       | Description                                                 | Type   | Available |
|------------|-------------------------------------------------------------|--------|:---------:|
| label      | Nouveau label de la base de données                         | string |     X     |
| dbname     | Nom de la base de données                                   | string |     X     |
| dbtype     | Driver Doctrine utilisé (pdo_pgsql, pdo_mysql, ...)         | string |     X     |
| dbhost     | Nom du serveur hôte de la base de données                   | string |     X     |
| dbport     | Port d'écoute                                               | int    |     X     |
| dblogin    | Nom d'utilisateur de connexion                              | string |     X     |
| dbpassword | Mot de passe de connexion                                   | string |     X     |

**Auth required** : Yes

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `200 Ok`

**Exemple de contenu**

```json
{
    "id": 1,
    "label": "New label",
    "dbname": "anis_test",
    "dbtype": "pdo_pgsql",
    "dbhost": "db",
    "dbport": 5432,
    "dblogin": "anis",
    "dbpassword": "anis"
}
```

<p class='error_response' markdown='1'>Error Response : </p>

**Code** : `404 Not Found`

**Exemple de contenu**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 45
        }
    ]
}
```

**Code** : `400 Bad Request`

**Exemple de contenu**

```json
{
    "message": "400 Bad Request",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpBadRequestException",
            "code": 400,
            "message": "Param dbpassword needed to edit the database",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 61
        }
    ]
}
```

### Supprimer une base de données par ID

Permet de supprimer une base de données métier dans ANIS

**URL** : `/database/{id}`

**Method** : `DELETE`

**URL Params** : 

| Name       | Description                 | Type   | Available |
|------------|-----------------------------|--------|:---------:|
| id         | ID de la base de données    | int    |     X     |

**Query Params** : None

**Auth required** : Yes

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `200 OK`

**Exemple de contenu**

```json
{
    "message": "Database with id 2 is removed!",
}
```

<p class='error_response' markdown='1'>Error Response : </p>

**Code** : `404 Not Found`

**Exemple de contenu**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/DatabaseAction.php",
            "line": 45
        }
    ]
}
```

### Récupérer la liste des tables dans une base de données

Permet de récupérer la liste des tables ou des vues présentes dans une base de données métier.

**URL** : `/database/{id}/table`

**Method** : `GET`

**URL Params** : 

| Name       | Description                | Type   | Available |
|------------|----------------------------|--------|:---------:|
| id         | ID de la base de données   | int    |     X     |

**Query Params** : None

**Auth required** : Yes

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `200 OK`

**Exemple de contenu**

```json
[
    "obs_cat",
    "aspic_vipers_dr2_w1",
    "observations_info",
    "aspic_gama_g02",
    "hk",
    "observations",
    "rawproducts",
    "products",
    "sp_cards",
    "sp_cards_bak",
    "vhf",
    "ancillary_files",
    "calibpack",
    "obspack",
    "observation",
    "public.v_products",
    "public.v_rawproducts",
    "public.anis_observation",
    "public.v_obspack"
]
```

<p class='error_response' markdown='1'>Error Response : </p>

**Code** : `404 Not Found`

**Exemple de contenu**

```json
{
    "message": "404 Not Found",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpNotFoundException",
            "code": 404,
            "message": "Database with id 2 is not found",
            "file": "/project/src/Action/TableListAction.php",
            "line": 58
        }
    ]
}
```

### Récupérer la liste des projets

Utilisé pour récupérer la liste des projets scientifiques disponibles dans ANIS.

**URL** : `/project`

**Method** : `GET`

**URL Params** : 

**Query Params** : None

**Auth required** : No

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `200 OK`

**Exemple de contenu**

```json
[
    {
        "name": "anis_project",
        "label": "Anis Project Test",
        "description": "Project used for testing",
        "link": "http://project.com",
        "manager": "M. Durand",
        "id_database": 1
    },
    {
        "name": "svom",
        "label": "Svom Project Test",
        "description": "Project used for testing",
        "link": "http://svom.com",
        "manager": "S. Basa",
        "id_database": 1
    },
    {
        "name": "colibri",
        "label": "Colibri Project Test",
        "description": "Project used for testing",
        "link": "http://colibri.com",
        "manager": "S. Basa",
        "id_database": 1
    }
]
```

### Ajouter un nouveau projet

Utilisé pour ajouter un nouveau projet scientifique dans ANIS.

**URL** : `/project`

**Method** : `POST`

**URL Params** : None

**Query Params** : None

**Body** : 

| Name        | Description                                  | Type   | Available |
|-------------|----------------------------------------------|--------|:---------:|
| name        | Nom d'identification du projet               | string |     X     |
| label       | Label de la nouvelle entrée du projet        | string |     X     |
| description | Description du projet                        | string |     X     |
| link        | Lien vers le site web du projet              | string |     X     |
| manager     | Chef de projet                               | string |     X     |
| id_database | ID de la base de données associée au projet  | string |     X     |

**Auth required** : Yes

<p class='success_response' markdown='1'>Success Response : </p>

**Code** : `201 Created`

**Exemple de contenu**

```json
{
    "name": "new_project",
    "label": "New project label",
    "description": "Project used for testing",
    "link": "http://project.com",
    "manager": "S. Dupont",
    "id_database": 1
}
```

<p class='error_response' markdown='1'>Error Response : </p>

**Code** : `400 Bad Request`

**Exemple de contenu **

```json
{
    "message": "400 Bad Request",
    "exception": [
        {
            "type": "Slim\\Exception\\HttpBadRequestException",
            "code": 400,
            "message": "Param id_database needed to add a new project",
            "file": "/project/src/Action/ProjectListAction.php",
            "line": 50
        }
    ]
}
```