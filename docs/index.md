# Bienvenue dans la documentation technique d'ANIS

![anis_v3_logo](img/anis_v3_logo300.png#center)

## Introduction

Bienvenue dans la documentation technique du logiciel `AstroNomical Information System`.

Ici vous pourrez trouver toutes les informations nécessaires pour installer et commencer le développement avec la dernière version d'ANIS.

ANIS est une application web qui est composé de plusieurs entités :  

 - `anis-server`: Il s'agit de la partie serveur qui est une API REST et qui communique uniquement en JSON. 
    - Elle permet à l'administrateur de configurer des projets scientifiques ainsi que des jeux de données.
    - Elle permet également aux utilisateurs d'effectuer des recherches.
 - `anis-client`: C'est la partie qui contient l'application web. Elle utilise `anis-server` pour fonctionner.
    - Le client propose une interface graphique (web) pour parcourir les projets scientifiques et rechercher des données.
    - Le client contient également un panneau d'administration pour configurer les projets et les jeux de données en mode graphique.
 - `anis-services` : C'est une API qui est utilisée par `anis-client` et qui permet la manipulation de fichiers astrophysiques (FITS) comme de la transformation de format ou du découpage d'images. Cette partie est codée en `Python` et utilise la bibliothèque de fonctions `Astropy`.

Vous pouvez retrouver ici le dépôt complet du code source d'ANIS : [https://gitlab.lam.fr/anis/anis-next](https://gitlab.lam.fr/anis/anis-next). 

## Fonctionnalités

 - ANIS fournit un API REST complète pour rechercher ou manipuler des données dans le domaine de l'astrophysique.
 - Avec l'API REST un administrateur peut configurer des jeux de données et un utilisateur peut effectuer des recherches.
 - ANIS fournit également une interface web qui permet aux utilisateurs de parcourir ou rechercher des données plus simplement qu'en utilisant l'API REST.
 - ANIS fournit également une interface web administrateur pour gérer l'ensemble de l'applications.

## Prérequis

Pour installer le logiciel en local sur votre machine vous aurez besoin de :

- `git`
- `Make`
- `Docker` et `docker-compose`

## A propos de ce logiciel

ANIS a été developpé par le `Laboratoire d'Astrophysique de Marseille` pour répondre aux besoins du laboratoire et des différents projets scientifiques dans lesquels il est engagé.

![lam_logo](img/lam_logo.jpg#center)

## Licence

`AstroNomical Information System` est sous licence CeCILL (Version 2.1 dated 2013-06-21).

Vous pouvez utiliser, modifier, et/ou redistribuer le logiciel en prenant en compte les termes de la licence
CeCILL. Voir plus d'informations en suivant le lien : [https://cecill.info](https://cecill.info).

## Auteurs

Ici vous pouvez trouver la liste des personnes impliquées dans le développement :

* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Chrystel Moreau` : Laboratoire d'Astrophysique de Marseille (CNRS)
* `Tifenn Guillas` : Laboratoire d'Astrophysique de Marseille (CNRS)