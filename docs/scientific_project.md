# Projet scientifique

## Introduction 

Dans ANIS l'administrateur va organiser et rassembler les jeux de données dans un projet scientifique.
Les projets scientifiques sont stockés dans la base de donées `metamodel` dans la table `instance`.

L'administrateur a la possibilité de personnaliser les pages d'un projet scientifique (couleur, liens, pages, images...)

## Portail

L'ensemble des projets scientifiques d'ANIS sont listés sur la page portail.
C'est la première page utilisateur de l'application ANIS.

## Admin - Lister les projets scientifiques

ANIS contient 1 ou plusieurs projets scientifiques et vous pouvez les lister en vous rendant 
sur l'interface d'administation : [http://localhost:4200/admin](http://localhost:4200/admin).

## Admin - Ajouter un nouveau projet scientifique

Pour ajouter un nouveau projet scientifique vous pouvez cliquer sur le bouton `+` et remplir le formulaire.

Voici une explication des diffèrentes informations requises dans le formulaire pour un projet scientifique : 

 - `Name`: C'est la clé primaire du projet. Le nom doit être unique et il sera utilisé dans l'API pour retrouver le projet.
 - `Label`: C'est le nom du projet tel qu'il apparaîtra dans l'interface graphique
 - `Data path`: Répertoire
 - `Instance color`: C'est la couleur qui sera utilisé dans les pages dédiées à ce projet scientifique
 - `Logo`: C'est le logo du projet scientifique. Le fichier du logo doit être selectionné dans le répetoire `Data path` du projet.
 - `Favicon`: C'est le logo favicon qui sera utilisé par le navigateur (dans l'onglet) pour le projet scientifique.
 - `Component`: Component Angular utilisé sur la page d'accueil du projet
 - `Text`: Texte d'introduction utilisé sur la page d'accueil du projet
 - `Home Logo`: Logo utilisé sur la page d'accueil du projet
 - `Classic search allowed`: Permet d'activer ou de désactiver la recherche par critére pour ce projet
 - `Classic search label`: Permet de personnaliser le label du lien du menu pour ce projet scientifique
 - `Search multiple allowed`: Permet d'activer ou de désactiver la recherche multi jeux de données
 - `Search multiple label`: Permet de personnaliser le label du lien du menu pour ce projet scientifique 
 - `All datasets with cone search enabled selected by default`: Tout les datasets seront sélectionnés par défault dans la recherche multi jeux de données
 - `Documentation allowed`: Permet d'activer ou de désactiver la fonctionnalité documentation pour ce projet scientifique
 - `Documentation label`: Permet de personnaliser le label du lien du menu vers la documentation

## Admin - Editer un projet scientifique

Il est possible de changer les informations relatives à un projet scientifique en cliquant sur l'icône d'édition d'un projet.
Par contre, vous ne pourrez par changer la propriètè `name` qui est la clé primaire de la table `instance`.

## Admin - Supprimer un projet scientifique

Vous pouvez également supprimer un projet scientifique en cliquant sur l'icône poubelle. 

**Attention**: supprimer le projet scientifique supprimera égalements toute la configuration sur le projet scientifique et 
les différents jeux de données associés.