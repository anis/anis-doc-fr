# Effectuer une recherche

## Introduction

Anis-server fournit un point d'entrée unique pour effectuer une recherche dans un jeu de données.
Pour une recherche dans un jeu de données, l'utilisateur peut spécifier :

 - Les attributs à afficher (colonnes)
 - Les critères de recherche
 - Ordre de tri
 - La pagination
 - Un format de sortie

URL d'exemple pour rechercher dans le jeu de données `observations` :

```
/search/observations
```

Les paramètres de recherche doivent être ajoutés après le `?`.
Nous verrons dans la documentation suivante quels sont les paramètres disponibles et comment les ajouter.

Exemple : 

```
/search/observations?[PARAMETERS]
```

`Attention` : le paramètre `a` est obligatoire pour effectuer une recherche.

## Les attributs (param a)

L'utilisateur peut sélectionner les attributs de sortie par leur ID attribué dans la base `metamodel`.
Lorsque le jeu de données est créé et que les attributs sont ajoutés, le `metamodel` attribue un numéro d'identification à partir de 1.
L'utilisateur peut lister les attributs disponibles pour un jeu de données : 

```bash
curl "http://localhost:8080/dataset/observations/attribute"
```

Pour ajouter une liste d'attributs dans une recherche de données, l'utilisateur doit ajouter le paramètre de requête `a`.
Il doit spécifier une liste d'identifiants séparés par un point-virgule.

Si l'utilisateur veut les attributs 1, 2 et 3 :

```
a=1;2;3
```

Exemple :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3"
```

L'utilisateur peut choisir l'ordre de sortie des attributs.
Par exemple, si l'utilisateur veut voir l'attribut 3 avant le 2 :

```bash
curl "http://localhost:8080/search/observations?a=1;3;2"
```

### Compter le nombre d'enregistrements

L'utilisateur peut remplacer la liste des attributs par le mot-clé spécial `count`.
Ceci retournera le nombre d'enregistrements trouvés pour une recherche.
Par exemple, si l'utilisateur veut récupérer le nombre d'enregistrements dans l'ensemble de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=count"
```

Si l'utilisateur veut récupérer le nombre d'enregistrements dans l'ensemble de données d'observations lorsque l'ID observations est supérieur à 424 :

```bash
curl "http://localhost:8080/search/observations?a=count&c=1::gt::424"
```

### Renvoyer tous les attributs

L'utilisateur peut remplacer la liste des attributs par le mot-clé spécial `all`.
Cela renverra tous les attributs disponibles pour l'ensemble de données.
Par exemple, si l'utilisateur souhaite récupérer toutes les colonnes de cet ensemble de données, il peut effectuer une recherche :

```bash
curl "http://localhost:8080/search/observations?a=all&c=1::gt::424"
```

## Criteria (param c)

L'utilisateur peut appliquer un ou plusieurs critères de recherche pour filtrer le résultat de la recherche.
Les critères de recherche sont ajoutés au paramètre `c` et chaque critère est séparé par un `;`.

```
c=c1;c2;c3....
```

Chaque critère de recherche est composé de 3 parties :

 - L'attribut sur lequel le filtre sera effectué.
 - L'opérateur
 - 0 ou n valeurs

Chaque partie doit être séparée par un `::`.

Exemple : 

```
1::eq::10
```

Ce filtre recherchera toutes les valeurs où l'attribut 1 est égale à 10.

Nous allons maintenant voir la liste des opérateurs disponibles.

### Equal

L'utilisateur peut ajouter un critère de recherche égal en utilisant l'opérateur `eq`.
Avec l'opérateur `eq`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur veut rechercher l'observation avec le numéro 418 :

```
1::eq::418
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::eq::418"
```

### Not equal

L'utilisateur peut ajouter un critère de recherche non égal en utilisant l'opérateur `neq`.
Avec l'opérateur `neq`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur veut rechercher toutes les observations sauf 418 :

```
1::neq::418
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::neq::418"
```

### Between

L'utilisateur peut ajouter un critère de recherche `between` en utilisant l'opérateur `bw`.
Avec l'opérateur `bw`, l'utilisateur doit spécifier deux valeurs séparées par `|`.
Par exemple, si l'utilisateur veut rechercher toutes les observations entre 418 et 420 :

```
1::bw::418|420
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::bw::418|420"
```

### Greater than

L'utilisateur peut ajouter un critère de recherche `supérieur` à l'aide de l'opérateur `gt`.
Avec l'opérateur `gt`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur veut rechercher toutes les observations dont l'ID est supérieur à 424 :

```
1::gt::424
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::gt::424"
```

### Greater than equal

L'utilisateur peut ajouter un critère de recherche `supérieur ou égal` en utilisant l'opérateur `gte`.
Avec l'opérateur `gte`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur veut rechercher toutes les observations dont l'ID est supérieur ou égal à 424 :

```
1::gte::424
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::gte::424"
```

### Less than

L'utilisateur peut ajouter un critère de recherche `inférieur` à en utilisant l'opérateur `lt`.
Avec l'opérateur `lt`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur veut rechercher toutes les observations dont l'ID est inférieur à 424 :

```
1::lt::424
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::lt::424"
```

### Less than equal

L'utilisateur peut ajouter un critère de recherche `inférieur ou égal` en utilisant l'opérateur `lte`.
Avec l'opérateur `lte`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur veut rechercher toutes les observations dont l'ID est inférieur ou égal à 424 :

```
1::lte::424
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::lte::424"
```

### Like

L'utilisateur peut ajouter un critère de recherche `similaire` en utilisant l'opérateur `lk`.
Avec l'opérateur `lk`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur souhaite rechercher les observations du nom de l'objet (M 15) qui contient la chaîne de caractères 15 :

```
7::lk::15
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3;7&c=7::lk::15"
```

### Not like

L'utilisateur peut ajouter un critère de recherche `non similaire` en utilisant l'opérateur `nlk`.
Avec l'opérateur `nlk`, l'utilisateur doit spécifier une seule valeur.
Par exemple, si l'utilisateur souhaite rechercher les observations d'un nom d'objet qui ne contient pas la chaîne 15 :

```
7::nlk::15
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3;7&c=7::nlk::15"
```

### In

L'utilisateur peut ajouter un critère de recherche en utilisant l'opérateur `in`.
Avec l'opérateur `in`, l'utilisateur doit spécifier plusieurs valeurs séparées par `|`.
Par exemple, si l'utilisateur souhaite rechercher les observations numéro 418, 419 et 420 :

```
1::in::418|419|420
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::in::418|419|420"
```

### Not in

L'utilisateur peut ajouter un critère de recherche `not in` en utilisant l'opérateur `nin`.
Avec l'opérateur `nin`, l'utilisateur doit spécifier plusieurs valeurs séparées par `|`.
Par exemple, si l'utilisateur souhaite rechercher les observations qui ne comportent pas les numéros 418, 419 et 420 :

```
1::nin::418|419|420
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&c=1::nin::418|419|420"
```

### Null

L'utilisateur peut ajouter un critère `null` en utilisant l'opérateur `nl`.
Avec l'opérateur `nl`, l'utilisateur ne doit pas spécifier de valeur.
Par exemple, si l'utilisateur veut rechercher des cartes dont le type est nul :

```
6::nl
```

Exemple complet avec jeu de données sp_cards :

```bash
curl "http://localhost:8080/search/sp_cards?a=1;2;3;4;5;6&c=6::nl"
```

### Not null

L'utilisateur peut ajouter un critère `not null` en utilisant l'opérateur `nnl`.
Avec l'opérateur `nnl`, l'utilisateur ne doit pas spécifier de valeur.
Par exemple, si l'utilisateur veut rechercher des cartes dont le type n'est pas null :

```
6::nnl
```

Exemple complet avec jeu de données sp_cards :

```bash
curl "http://localhost:8080/search/sp_cards?a=1;2;3;4;5;6&c=6::nnl"
```

### Json

L'utilisateur peut ajouter un critère json en utilisant l'opérateur `js`.
Cet opérateur est un opérateur spécial qui permet de rechercher dans **une colonne json pour les bases de données PostgreSQL uniquement**.
Par exemple, si l'utilisateur veut rechercher des cartes lorsque le json_schema contient la clé `name` égale à `MXT-EVT-CAL` :

```
7::js::name|eq|MXT-EVT-CAL
```

Exemple complet avec jeu de données sp_cards :

```bash
curl "http://localhost:8080/search/sp_cards?a=1;2;3;4;5;6;7&c=7::js::name|eq|MXT-EVT-CAL"
```

## Cone-search (param cs)

L'utilisateur a la possibilité d'effectuer une recherche conique sur un ensemble de données pour filtrer les résultats.
L'utilisateur doit spécifier une position spatiale en degrés et un rayon de recherche en secondes d'arc.

Le paramètre `cs` requiert 3 valeurs séparées par `:` :

 - Ascension droite en degrés
 - Déclinaison en degrés
 - Rayon de recherche en secondes d'arc

Par exemple, si l'utilisateur veut toutes les observations autour de la position 322.5 +12.167 (100 secondes d'arc) :

```
cs=322.5:12.167:100
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&cs=322.5:12.167:100"
```

Avertissement : Pour effectuer une recherche via un cone-search dans un ensemble de données, la configuration de l'ensemble de données doit contenir des paramètres cone-search 
et le drapeau cone-search enabled doit être à true.

Exemple voir `config->cone-search` pour les observations de l'ensemble de données :

```bash
curl "http://localhost:8080/dataset/observations"
```

## Trier les résultats (param o)

L'utilisateur a la possibilité de trier les résultats par attributs et par ordre croissant ou décroissant.
L'utilisateur peut effectuer plusieurs tris. Par exemple, trier par date, puis pour chaque date par ordre alphabétique.

Chaque ordre `o` doit contenir :

 - ID de l'attribut sur lequel le tri est effectué
 - L'ordre du tri, `a` pour ascendant et `d` pour descendant

S'il y a plusieurs commandes, chaque tri doit être séparé par `;`.

Par exemple, si l'utilisateur veut trier toutes les observations par date et, pour chaque date, par nom d'objet en ordre décroissant :

```
o=4:a;7:d
```

Exemple complet avec jeu de données d'observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3;4;5;6;7&o=4:a;7:d"
```

## Pagination (param p)

L'utilisateur a la possibilité de retourner le résultat en utilisant une pagination.
Ceci est très utile lorsque le résultat est trop grand.
L'utilisateur peut choisir le nombre de résultats par page et la page à afficher.

Le paramètre `p` nécessite 2 valeurs :

 - Le nombre de résultats par page
 - La page à afficher

Par exemple, si l'utilisateur veut voir la première page avec 2 résultats par page :

```
p=2:1
```

La pagination nécessite de classer le résultat par exemple par ID des observations.

Exemple complet avec le jeu de données des observations :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&o=1:a&p=2:1"
```

## Format des résultats

L'utilisateur peut choisir le format des résultats de la recherche avec le paramètre `f`.
Par défaut, le résultat est retourné dans un format json.

L'utilisateur peut choisir entre 4 formats : 

- `json`
- `csv`
- `ascii` (valeurs séparées par un espace)
- `votable`

Par exemple, si l'utilisateur veut toutes les observations au format csv :

```bash
curl "http://localhost:8080/search/observations?a=1;2;3&f=csv"
```
