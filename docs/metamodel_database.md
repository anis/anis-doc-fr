# Base de données `Metamodel`

## Introduction

Pour fonctionner ANIS à besoin d'au moins deux bases de données : 

 - Une base de données pour stocker l'ensemble de la configuration(`metamodel database`)
 - Au moins une base de données qui contient les jeux de données métiers sur lesquels les utilisateurs vont effectuer leurs recherches.

Ici nous allons expliquer comment fonctionne la base de données de configuration, c'est à dire la `metamodel database`.

## Enjeux de cette base de données

ANIS à besoin d'une base de données pour stocker toute la configuration :

 - Les différents bases de données métiers disponibles et contenant les catalogues de données.
 - Les différents projets scientifiques et leurs configurations.
 - Les jeux de données, leurs attributs et toutes la configuration.
 - Les droits d'accès sur les jeux de données pour les utilisateurs.

## Schéma de la base

![database_schema](img/metamodel_mcd.svg)

## Configuration ANIS

Par défaut le conteneur docker du serveur est configuré pour se connecter sur une base de données metamodel en `PostgreSQL` créée et lancée par le `docker-compose`.
En effet, le `docker-compose` intégre un conteneur avec un sytème de gestion de base de données (Postgres). Ce conteneur se nomme `db`.
Pour voir la configuration, vous pouvez vous rendre dans le fichier `docker-compose.yml` à la racine du projet.

Toute la configuration du serveur ANIS se fait via des variables d'environnements transmises au conteneur.

![php_settings.png](img/php_settings.png#center)

Ici nous allons lister et expliquer la configuration qui concerne la connexion vers la base de données de configuration `metamodel` :

- `DATABASE_DEV_MODE`: `1` est la valeur en mode développement et `0` en mode production
- `DATABASE_CO_DRIVER`: Il faut renseigner le driver PDO Dotrine utilisé pour contacter la base de données
- `DATABASE_CO_HOST`: Le nom de l'hôte du serveur de base de données
- `DATABASE_CO_PORT`: Le port d'écoute du serveur de base de données
- `DATABASE_CO_DBNAME`: Le nom de la base de données `metamodel` sur la laquelle se connecter
- `DATABASE_CO_USER`: Le nom d'utilisateur de connexion utilisé
- `DATABASE_CO_PASSWORD`: Le mot de passe de connexion

Vous êtes libre de changer la configuration pour vous connecter sur une autre base de données `metamodel`.

## Entités Doctrine

ANIS serveur utilise une bibliothéque de fonctions appelée Doctrine pour générer la base de données `metamodel` à partir du code.
Vous pouvez trouver plus d'informations à propos de Doctrine sur le site officiel : [https://www.doctrine-project.org](https://www.doctrine-project.org).

ANIS serveur stocke les différentes entités Doctrine dans le dossier `server/src/Entity`. Chaque fichier correspond à une table dans la base de données `metamodel`.

![doctrine_entities](img/doctrine_entities.png)

Si vous souhaitez changer la structure de la base de données vous devez éditer ou ajouter des entités dans ce dossier.
Attention car pour chaque changement de structure vous devez régénérer ou mette à jour la base de données ainsi que les fichiers Doctrine proxies.

## Outil en ligne de commande Doctrine

Vous pouvez utiliser l'outil en ligne de commande de Doctrine pour gérer la plupart des opérations.
Pour lancer l'outil, ANIS doit être démarré.

Vous devez vous trouver à la racine de l'application ANIS.
Puis vous devez entrer dans le conteneur serveur en tappant simplement la comande suivante : 

```bash
$ make shell_server
```

Une fois dans le conteneur vous pouvez taper la commande suivante pour exécuter l'outil en ligne de commande Doctrine : 

```bash
$ ./vendor/bin/doctrine
```

Ce que vous voyez sont les différentes opérations que vous pouvez effectuer :

![doctrine_cli_operations](img/doctrine_cli_operations.png)

## Valider le schéma

Si vous avez effectué des opérations de changement sur les fichiers entités Doctrine vous devez vous assurer que le schéma est valide.
Doctrine propose une commande pour s'assurer de la validité du schéma : 

```bash
$ ./vendor/bin/doctrine orm:validate-schema
```

## Créer la base de données

Si les tables correspondant au schéma de votre base de données ne sont pas encore générées vous pouvez demander à Doctrine de le faire pour vous avec 
la commande suivante : 

```bash
$ ./vendor/bin/doctrine orm:schema-tool:create
```

Attention la base de données doit être vierge pour effctuer cette opération.

## Supprimer la base de données

Si les tables de votre base de données `metamodel` sont déjà générées et que vous voulez repartir sur une base vierge vous pouvez taper la commande suivante : 

```bash
$ ./vendor/bin/doctrine orm:schema-tool:drop
```

## Mettre à jour la base de données

Si vous avez effectué des changements sur les entités vous pouvez mette à jour la structure de la base en tapant la commande suivante : 

```bash
$ ./vendor/bin/doctrine orm:schema-tool:update
```

## Générer les Doctrine proxies

Si vous avez effectué des changements sur les entités Doctrine vous devez également mettre à jour les fichiers proxies.
Les fichiers entités proxies sont utilisés pour améliorer les performances des requêtes vers la base.

Pour régénerer les fichiers vous devez taper la commande suivante : 

```bash
$ ./vendor/bin/doctrine orm:generate-proxies
```
