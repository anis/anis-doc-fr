# Jeu de données

## Introduction

Chaque projet scientifique contient un ou plusieurs jeux de données dans lesquels les
utilisateurs vont pouvoir effectuer des recherches.

## Admin - Lister les jeux de données

Vous pouvez lister les jeux de données disponibles pour un projet scientifique.
Pour cela rendez-vous dans l'interface d'administration et cliquez sur le bouton de configuration d'un projet scientifique.

Les jeux de données sont organisés par famille et chaque cadre représente une famille.
Dans chaque famille vous pouvez retrouver un ou plusieurs jeux de données.

## Admin - Ajouter un nouveau jeu de données

Pour ajouter un nouveau jeu de données dans une famille, vous pouvez cliquer sur le bouton `+` présent dans le cadre d'une famille.

## Admin - Editer un jeu de données

## Admin - Supprimmer un jeu de données