# Bases de données métier et surveys

## Introduction

ANIS peut se connecter à une ou plusieurs bases de données métier. Chaque base de données peut contenir des tables
ou des vues qui sont susceptibles de devenir des jeux de donées.

Pour pouvoir être utilisable dans ANIS une base de données métier doit être liée avec un `survey`.

![databases_mcd](img/databases_mcd.png#center)

## Admin - Lister les bases de données métier

Vous pouvez lister les bases de données métier en vous rendant dans l'interface d'administration 
dans l'onglet Databases : [http://localhost:4200/admin/database/database-list](http://localhost:4200/admin/database/database-list)

![databases_list](img/databases_list.png#center)

## Admin - Ajouter une base de données métier

Cliquez sur le bouton `New database` pour ajouter une base de données métier dans ANIS.

![add_database](img/add_database.png#center)

Voici une explications des différents champs du formulaire : 

### Label

Nom de la base de données tel qu'il sera affiché dans l'interface graphique.

### Name

Nom de la base de données

### Type

Driver utilisé pour la connexion vers la base de données

### Host

Nom d'hôte du serveur de base de données

### Port

Port d'écoute du serveur de base de données

### Username

Nom d'utilisateur de connexion vers la base de données

### Password

Mot de passe de connexion vers la base de données

## Admin - Editer une base de données métier

1. Cliquez sur le bouton d'édition et changer un ou plusieurs champs du formulaire.
2. Cliquez sur le bouton `Update database information`

## Admin - Supprimer une base de données métier

**Attention**: Vous ne pouvez pas dans ANIS supprimer une base de données métier qui est utilisé par un ou plusieurs `survey`.

Pour supprimer une base de données métier dans ANIS il suffit de cliquer sur l'icône poubelle en rouge.

## Admin - Lister les surveys disponibles

Vous pouvez lister les surveys en vous rendant dans l'interface d'administration 
dans l'onglet Surveys : [http://localhost:4200/admin/survey/survey-list](http://localhost:4200/admin/survey/survey-list)

![surveys_list](img/surveys_list.png#center)

Ici vous pouvez voir que les trois surveys sont liés à la base `Test`.

## Admin - Ajouter un survey

Cliquez sur le bouton `New survey` pour ajouter un survey dans ANIS.

![add_survey](img/add_survey.png#center)

Voici une explications des différents champs du formulaire : 

### Name

C'est le nom du survey dans ANIS et c'est aussi la clé primaire utilisée par l'API REST pour identifier ce survey.
Le nom est unique dans toute l'application. Il doit être en miniscule sans caractères spéciaux.

### Label

C'est le nom qui est utilisé pour l'affichage du survey dans l'interface graphique. Il peut contenir des masjuscules et des caractères spéciaux.

### Database

Il s'agit de la base de données métier utilisée pour ce survey.

### Description

Permet d'ajouter des informations à propos du survey.

### Link

Lien vers le site Internet du survey

### Manager

Nom de la personne en charge du survey

## Admin - Editer un survey

1. Cliquez sur le bouton d'édition et changer un ou plusieurs champs du formulaire.
2. Cliquez sur le bouton `Update survey information`

## Admin - Supprimer un survey

**Attention**: Vous ne pouvez pas dans ANIS supprimer un survey qui est utilisé par un ou plusieurs jeux de données.

Pour supprimer un survey dans ANIS il suffit de cliquer sur l'icône poubelle en rouge.