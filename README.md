# ANIS-DOC

## Introduction

Welcome to the ANIS user documentation repository. This manual is written using the mkdocs tool (https://www.mkdocs.org/).

## Installation

You will need the local image containing the mkdocs tool to use this repository.

- To build the image : `make install`

## Running dev-server

MkDocs comes with a built-in dev-server that lets you preview your documentation as you work on it.

- To run the dev-server : `make start`

## Building documentation website

The website is generated into a folder named `site`

 - To build the documentation website : `make build`

## Authors

* `François Agneray` : Laboratoire d'Astrophysique de Marseille (CNRS)

## Web interface

After the start the interface is available at: [http://localhost:8888](http://localhost:8888)
